---
layout: markdown_page
title: "Category Direction - Runner SaaS"
description: "This is the Product Direction Page for the Runner SaaS product category."
canonical_path: "/direction/verify/runner_saas/"
---

## Runner SaaS

| -                     | -                              |
| Stage                 | [Verify](/direction/verify/)   |
| Maturity              | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2023-12-15`                   |

### Introduction

Thanks for visiting this direction page on the Runner SaaS category at GitLab. This page belongs to the [Runner SaaS Group](https://about.gitlab.com/handbook/product/categories/#runner-saas-group) within the Verify Stage and is maintained by [Gabriel Engel](mailto:gengel@gitlab.com). Feel free to follow our [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqcTxGhFdJtcUpvjdf05MHY) where we will upload walkthroughs of direction page updates and iteration kickoffs.

### Strategy and Themes

With the adoption of a DevSecOps approach and the resulting gains in developer and delivery efficiency, organizations are seeing an increased demand for computing power to run CI/CD pipelines.

Our **vision** is to provide a fully managed, best-in-class hosted CI/CD build infrastructure that is fast and secure by default. We want to eliminate the overhead of hosting and maintaining your build infrastructure, ultimately enabling you to deliver software faster. Our platform approach will allow you to run cross-platform (Docker containers on Linux, Windows, macOS) builds in one CI/CD pipeline. With GitLab SaaS Runners, we aim to help you realize increased cost savings and efficiency by providing a reliable service for running all CI/CD builds that do not need to run on your infrastructure.

Today, the [SaaS runners](https://docs.gitlab.com/ee/ci/runners/) product offerings are:

- [SaaS runners on Linux - GA](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html)
- [GPU-enabled SaaS runners - GA](https://docs.gitlab.com/ee/ci/runners/saas/gpu_saas_runner.html)
- [SaaS runners on macOS - Beta](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)
- [SaaS runners on Windows - Beta](https://docs.gitlab.com/ee/ci/runners/saas/windows_saas_runner.html)

### 1 year plan

The primary FY2025 runner themes focus on building a competitive and complete CI/CD offering that allows customers to build for all common platforms.

- [CI Steps (GA)](https://gitlab.com/groups/gitlab-org/-/epics/11525) to support both [CI Components Catalog](https://about.gitlab.com/direction/verify/component_catalog) and CI Events
- [GRIT - Linux Docker template on AWS (GA)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/2) to enable hosted runners on GitLab Dedicated
- [SaaS runners on macOS](https://gitlab.com/groups/gitlab-org/-/epics/8267) transition to General Availability
- [Add ARM compute to SaaS runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8442)
- [Larger SaaS runners on macOS](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/107)
- [SaaS runners on Windows](https://gitlab.com/groups/gitlab-org/-/epics/2162) transition to General Availability

In addition we want to reduce operational overheads by dogfooding our new autoscaler & GRIT for our fleet of runners.
- [Transition SaaS runners on Linux to new Autoscaler](https://gitlab.com/groups/gitlab-org/ci-cd/shared-runners/-/epics/17)
- [Establish GitLab Runner Infrastructure Toolkit (GRIT) for GCP & AWS](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/1) and transition our fleet to GRIT

### 3 year plan

In the next three years, we want to GitLab-hosted runners as the fastest and most cost-efficient CI in the market. Our efforts include [best-in-class ci build speed](https://about.gitlab.com/company/team/structure/working-groups/ci-build-speed/), [custom-hosted runners](https://gitlab.com/groups/gitlab-org/-/epics/10073), and a new pricing model. Users will be able to request [custom GitLab-hosted runners via GitLab UI](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/148), fully dedicated to their project, with more advanced features such as [dedicated IP-range](https://gitlab.com/gitlab-org/gitlab/-/issues/364425), [SSH access for debugging](https://gitlab.com/groups/gitlab-org/-/epics/10423) at a competetive price point, enabling enterprise organisations to fully focus on developing software without the hastle of managing infrastructure.

### Current scope

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- [CI Steps (Experiment)](https://gitlab.com/groups/gitlab-org/-/epics/11736) with a [video demo](https://www.youtube.com/watch?v=TU_53vWVKeE) showcasing how to use CI Steps
- [GRIT - Linux Docker template on AWS (Experiment)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/2) to enable hosted runners on GitLab Dedicated
- [Image support for macOS 14 (Sonoma) and Xcode 15](https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/#macos-14-sonoma-and-xcode-15-image-support)
- [Image support for macOS 13 (Ventura)](https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/#macos-13-ventura-image-for-saas-runners-on-macos)
- Adding `xlarge` and `2xlarge` runners on Linux to [offer more powerful compute](https://gitlab.com/groups/gitlab-org/-/epics/8714)

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

- We are working on [GRIT - Linux Docker template on AWS (Beta)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/2) to enable hosted-runners for GitLab Dedicated
- We are transitioning our SaaS runners on Windows (Beta) to [Windows Server 2022](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/152) as Windows 2019 reaches EOL
- We are focused to bring our [Apple silicon (M1) runners on macOS](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html) to [General Availability](https://gitlab.com/groups/gitlab-org/-/epics/8267)

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next three months we plan to complete the following projects:

- [GRIT - Linux Docker template on AWS (Beta)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/1) to support GitLab hosted runners for [GitLab Dedicated](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/)
- [CI Steps (Beta)](https://gitlab.com/groups/gitlab-org/-/epics/11525)
- [Apple silicon (M1) runners on macOS](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html) to [General Availability](https://gitlab.com/groups/gitlab-org/-/epics/8267)
- [Transition GitLab SaaS Runners on Linux to new Autoscaler](https://gitlab.com/groups/gitlab-org/ci-cd/shared-runners/-/epics/17)

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities

Cloud-native CI/CD solutions, such as GitLab.com, Harness.io, CircleCI, and GitHub, allow to run CI/CD pipelines on a fully managed runner fleet with no setup required.

In addition to eliminating CI build server maintenance costs, there are other critical considerations for organizations that can migrate 100% of their CI/CD processes to a cloud-native solution. These include security, reliability, performance, compute types, and on-demand scale.

CI build speed or time-to-result, and the related CI build infrastructure cost efficiency are critical competitive vectors. [CircleCI](https://circleci.com/circleci-versus-github-actions/) and [Harness.io](https://www.harness.io/blog/announcing-speed-enhancements-and-hosted-builds-for-harness-ci) are promoting CI-build performance in their go-to-market strategy.
Both [GitHub](https://github.blog/2022-12-08-experiment-the-hidden-costs-of-waiting-on-slow-build-times/) and [Harness.io](https://www.harness.io/blog/fastest-ci-tool) explored the cost impact of CI build performance measured by build times on hosted solutions.

#### Top Competitive Solutions

**Hosted Linux Compute**

| Size     | Machine Specs       | GitLab        | GitHub    | CircleCI      |
| -------- | ------------------- | ------------- | --------- | ------------- |
| small    | 2 vCPUs, 8GB RAM    | Available     | Available | Available     |
| medium   | 4 vCPUs, 16GB RAM   | Available     | Beta      | Available     |
| large    | 8 vCPUs, 32GB RAM   | Available     | Beta      | Available     |
| x-large  | 16 vCPUs, 64GB RAM  | Available     | Beta      | Available     |
| 2x-large | 32 vCPUs, 128GB RAM | Available     | Beta      | Not available |
| 3x-large | 48 vCPUs, 192GB RAM | Not available | Beta      | Not available |
| 4x-large | 64 vCPUs, 256GB RAM | Not available | Beta      | Not available |

**Hosted GPU Compute**

| Size       | Machine Specs       | GitLab        | GitHub        | CircleCI      |
| ---------- | ------------------- | ------------- | ------------- | ------------- |
| lite       | Nvidia Tesla P4     | Not available | Not available | Available     |
| standard   | Nvidia Tesla T4     | Available     | Not available | Available     |
| premium    | Nvidia Tesla V100   | Not available | Not available | Available     |

**macOS - Offer Positioning and Hosted Build Machines**

| | GitLab | GitHub | Xcode Cloud | CircleCI | Bitrise.io |
|-|--------|--------|------------------------|----------|------------|
| Positioning statement | SaaS runners on macOS provide an on-demand macOS build environment fully integrated with GitLab CI/CD. | A GitHub-hosted runner is VM hosted by GitHub with the GitHub actions runner application installed. | A CI/CD service built into Xcode, designed expressly for Apple developers. | Industry-leading speed. No other CI/CD platform takes performance as seriously as we do. | Build better mobile applications, faster. |
| Value proposition | You can take advantage of all the capabilities of the GitLab single DevOps platform and not have to manage or operate a build environment. | When you use a GitHub-hosted runner, machine maintenance and upgrades are taken care of.|Build your apps in the cloud and eliminate dedicated build infrastructure.| The macOS execution environment allows you to test, build, and deploy macOS and iOS apps on CircleCI. | CI for mobile - save time spent on testing, onboarding, and maintenance with automated workflows and triggers |
| Available machines | Apple silicon (M1): medium | Apple silicon (M1): large; x86-64: small, large | n/a | Apple silicon (M1): medium, large; x86-64: medium, x-large | Apple silicon (M1): medium, large; x86-64: medium, large, x-large |

---
layout: markdown_page
title: "Category Direction - Switchboard"
---

- TOC
{:toc}

## Switchboard

<!--
| | |
| --- | --- |
| Stage | [<STAGE-NAME>](/direction/<STAGE-NAME>/) |
| Maturity | [<MATURITY-LEVEL>](/direction/maturity/) |
| Content Last Reviewed | `yyyy-mm-dd` |
-->

### Introduction and how you can help

This page outlines the Direction for the GitLab Switchboard category which belongs to the [GitLab Dedicated](https://about.gitlab.com/handbook/product/categories/#gitlab-dedicated-group) group. To provide feedback or ask questions about this product category, reach out to the [Product Manager](mailto:lbortins@gitlab.com).

You can contribute to this category by:

- Commenting on a relevant epic or issue in [our project](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team) (GitLab internal), or opening a new issue in the [GitLab Dedicated issue tracker](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new?issue%5Bmilestone_id%5D=).
- Joining the discussion in the [#f_gitlab_dedicated](https://gitlab.slack.com/archives/C01S0QNSYJ2) Slack channel

To learn more about concepts and terminology discussed in this direction page, please refer to the [Switchboard Glossary](./glossary).

## Overview

### Solution

Switchboard is a portal for the [GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/) single-tenant SaaS offering. Switchboard is used both by GitLab Dedicated customers and internal GitLab teams who support and maintain GitLab Dedicated instances. 

While GitLab Dedicated customers can rely on GitLab to maintain the stability and security of their environment, tenant operators and system administrators can use Switchboard to quickly make configuration changes to their GitLab Dedicated tenant instances. Switchboard also serves as the "front door" to GitLab Dedicated by providing a welcoming and efficient onboarding experience.

Our north star for Switchboard is to reduce time to value (TTV) for new Dedicated customers and create internal efficiencies by minimizing the assistance tenant operators need from GitLab to onboard and manage their instances.

### Why is this important

Maturing Switchboard is key to the success of GitLab Dedicated at scale that delivers the unique value of a single-tenant SaaS environment that is able to meet their needs for security and compliance without the operational burden of a self-managed environment. 

From an internal perspective, Switchboard serves as the primary connection between internal operators and GitLab Dedicated tenant instances - internal operators do not have direct access to customer tenant environments ([read more about that here](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#access-controls)), and Switchboard enables them to operate at scale.

### Background

During the [Beta and LA phases](https://about.gitlab.com/direction/saas-platforms/dedicated/#limited-availability-roadmap), all management of tenant instances was performed by the same internal teams who are responsible for developing the architecture and automation needed to fully scale GitLab Dedicated as a product offering. While some operations will continue to be handled by these teams, there are many configurations and actions that Switchboard empowers tenant operators to complete on their own, such as:
- Reviewing their organization's SAML configurations
- Configuring IP allowlists
- Adding custom certificates

### Target Audience

Switchboard shares its target audience with [GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/#target-customer). The primary user persona for Switchboard is [Sidney (systems administrator)](https://about.gitlab.com/handbook/product/personas/#sidney-systems-administrator).

Within GitLab, Switchboard is used by the tenant operators, support, and professional services teams.

### Success Criteria

We are still finalizing our success criteria for Switchboard. At a high level, we know that we will want to measure our impact on both customer satisfaction (e.g. decrease in Time to Value with self-serve onboarding) as well as internal efficiency (e.g. decrease time per operator spent on tenant maintenance tasks).

### Current state

The Switchboard application is generally available since 2023-09-29 and is being used by GitLab operators supporting existing GitLab Dedicated tenant instances. Most of what they need to do (maintenance windows, incident response/remediation) is currently being completed with the Switchboard application and monitoring tools within the tenant instances themselves. 

The UI currently provides the ability to onboard new tenants as well as to make changes to existing tenants using the Switchboard Configuration UI (available to all users) or editing the tenant model JSON directly (available to [GitLab operators](https://about.gitlab.com/direction/saas-platforms/switchboard/glossary/#gitlab-operator) only). 

User management and audit logs for Amp jobs (internal only) are also available using the UI.

## Where are we headed

As more tenant administrators are invited to use Switchboard, GitLab internal operators will have significantly fewer interactions with individual instances and will instead focus on monitoring and maintenance across all Dedicated tenants with visibility provided by Switchboard. 

Switchboard will also enable tenant operators to monitor the health of their instances and provide them with critical insights should they need to reach out to GitLab support.

### What is next for us

The next step for Switchboard is to expand the functionality of the Configuration UI to empower tenant administrators to further configure their tenant instance and reduce the need for support requests.

As the Switchboard onboarding flow is used by more users, we will identify improvements and add further configuration options to reduce the need for manual intervention by SREs. 

As we do this, we will need to balance how each deliverable contributes to the north star of self-service and efficiency with the potential to cause an incident that will be time-consuming for GitLab support and/or operators to remediate.

Features that allow GitLab operators to scale up their capacity for supporting tenant environments will take first priority.

Work in progress and in the backlog is being tracked on the [Switchboard Build Board](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/boards/4498935?label_name%5B%5D=component%3A%3ASwitchboard), which is internal only.

### What we are currently working on

Our current focus is expanding the Configuration UI to include Reverse Private Link and Private Hosted Zone configuration along with expanding the Switchboard onboarding flow to automatically share root credentials upon tenant instance creation. 

### What we recently completed

The Switchboard onboarding flow was released as generally available on 2023-09-29 and the Configuration UI was released to tenant administrators on 2023-10-31.
### What is Not Planned Right Now

* Switchboard will not directly interact with customer tenant environments. Read more about that in the [GitLab Dedicated documentation](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#access-controls).

* Switchboard is solely focused on supporting GitLab Dedicated instances. We do not currently plan to expand it to support GitLab.com or Self-Managed instances.

### Roadmap

In a future iteration, we will look to integrate with CustomersDot to minimize or even eliminate the manual work needed post-sales for customers to create their Dedicated environment as part of the onboarding process.

The remainder of our roadmap will be largely shaped by feedback from both tenant operators and GitLab operators as we move into GA. 

We have started gathering feature requests via this [feedback issue](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2078) (internal).

### Comparative Analysis

Similar portals have been built for other products used by Switchboard's target audience including ElasticSearch, MongoDB cloud, AWS and IBM cloud. We will look to common patterns shared by these portals as we continue to build out the capabilities of Switchboard.

---
layout: markdown_page
title: "Category Direction - Error Tracking"
description: "Error Tracking proactively surfaces application errors in developer's code so they can identify and fix them"
---

- TOC
{:toc}

## Overview

* Group: [Monitor:Observability](https://about.gitlab.com/direction/monitor/observability/)
* Maturity: [Minimal](https://about.gitlab.com/direction/maturity/)
* Content Last Reviewed: `2023-12-07`

This page outlines the direction for the Error Tracking category. Feedback is welcome: you can comment on the [feedback issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2362 "💬 Error Tracking User Feedback issue") or reach out to this page maintainer if you have any questions or suggestions.

## Error Tracking Overview

With Error Tracking, developers can identify and visualize errors (also known as exceptions or defects) triggered by end users while using their applications. It enables development teams to centralize all errors in one place for triage and resolution. And by integrating debugging workflows into their existing DevOps platform, it eliminates the need for context-switching between multiple tools and reduce loss of information.

## Features

| Features | Maturity | Description |
|----------|----------|-------------|
| Error Collection | Minimal | Uses Sentry SDK to collect errors from major Back-end and Front-end programming languages. |
| Error Grouping (Fingerprinting) | Minimal | Aggregate individual 'Error Events' experienced by multiple users into a single 'Error'. |
| View Stack Trace | Minimal | Visualize the source code responsible for the error, accompanied by a comprehensive list of method calls. |
| Triage | Minimal | Display all errors in a centralized, searchable list with triage options: ignore, resolve, or create an issue. |
| [Filter by labels](https://gitlab.com/gitlab-org/gitlab/-/issues/421745) | Not yet available | Filter errors via user-defined labels such as 'environment' (dev or production) |
| [Alert Creation](https://gitlab.com/gitlab-org/gitlab/-/issues/423913) | Not yet available | Automatically create alerts for new errors, to notify a user or a Slack |

Learn more:

* [Demo Video](https://www.youtube.com/watch?v=nEWPEqgB7iE)
* [Documentation page](https://docs.gitlab.com/ee/operations/error_tracking.html)

## Vision

Our objective is to increase developers' productivity by reducing the time they spend on identifying and resolving application errors, leveraging the GitLab platform to provide a superior experience to existing point solutions.

To accomplish this and reach a viable level of [maturity](https://about.gitlab.com/direction/maturity/), we will focus on developing three strategic axes:

1. **Close the loop between development and production**

   GitLab is already the place where developers build, test, and deploy code changes. By feeding production errors back into these workflows and linking errors to a particular commit, MR or release, we will enable developers to further "shift-left" production debugging, and tackle these issues more efficiently. To achieve that, we are considering integrating errors within existing IDE extensions, source code management, planning/issues and CI/CD pipelines UI.
2. **Provide end-to-end visibility for troubleshooting**

   By integrating with other telemetry data, error tracking will serve as an entry point to troubleshoot and dive deeper into related logs, metrics, or traces to find the root cause of the issue faster. To achieve that, we are considering replacing Sentry SDK by a collection system based on OpenTelemetry.
3. **Automate resolution with suggested fixes**

   By managing source code and integration/deployment workflows, GitLab has a unique knowledge of applications. We aim to leverage these insights to offer automated suggestions or directly apply fixes when new errors are detected. This will further reduce or eliminate the need for manual intervention in error resolution. See [related experiment](https://www.youtube.com/watch?v=GskfGIiQxBA).

## Target audience and use cases

* Error Tracking primary targets Software Developers ([Sasha](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer)). Errors in the front-end or back-end services are traditionally triaged by the development team owning the code as part of the debugging and improvement process.
* Errors Tracking may also be used by Platform Engineers ([Priyanka](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)) or SRE/Application Ops ([Allison](https://about.gitlab.com/handbook/product/personas/#allison-application-ops)) when investigating issues - for example to look for spikes in errors on production components when responding to an incident, to understand what caused the issue.

## What has been released recently?

**Error Tracking is now Generally Available for SaaS (16.0 - 2023-05-22)**

In this release, we are supporting both the [GitLab integrated error tracking](https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking) and the [Sentry-based](https://docs.gitlab.com/ee/operations/error_tracking.html#sentry-error-tracking) backends.

Learn more: [Release Post](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#error-tracking-is-now-generally-available)

## What's Next?

Currently (as of December 2023), the Observability Group is focused on [other categories](https://about.gitlab.com/direction/monitor/observability/#current-categories). As we progress on our [1 year plan](https://about.gitlab.com/direction/monitor/observability/#whats-our-1-year-plan), we will be able to redirect engineering effort on further improving Error Tracking.

## User Success Metrics

We will know we are on the right trajectory for Error Tracking when we are able to observe the following:
* Increase in namespaces/projects with [Integrated Error Tracking enabled](https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking).
* Increase in CMAU.
* Broad adoption across internal engineering teams (i.e. we are [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding)) of GitLab Integrated Error Tracking in addition or replacement of our existing point solution (Sentry).

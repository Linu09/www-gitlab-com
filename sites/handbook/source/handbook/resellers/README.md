# resellers has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/resellers](https://handbook.gitlab.com/handbook/resellers)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/resellers](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/resellers)

If you need help or assitance with this please reach out to @jamiemaynard (Developer/Handbooks) or
@marshall007 (DRI Content Sites).  Alternatively ask your questions on slack in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)


